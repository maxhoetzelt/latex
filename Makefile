DOCUMENT_NAME = main
OUTPUT_DIR = output
HANDIN_DIR = ../handin/

####latexmk

# Build the LaTeX document with latexmk
all: report

# Remove output directory and generated document
clean:
	rm -rf $(OUTPUT_DIR)
	rm -f $(DOCUMENT_NAME).pdf
	rm -f $(DOCUMENT_NAME).synctex.gz

# cleanup tempfiles
cleanup:
	latexmk -c

# Generate PDF output from LaTeX input files
report:
	latexmk
	#make cleanup
