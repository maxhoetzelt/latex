# LaTeX #

LaTeX is used to easily write scientific papers. This is a Template for that.

## Setup ##

* Clone this repository
* Install latexmk and make sure needed packages for texlive (languages etc.) are installed

## Usage ##
Make necessary changes in `settings.tex`. Add chapters to the Folder `contents/` named `<xx>chapter.tex`, where `<xx>` is a two-digit number. To change the logo of the company change `images/logo.png`. Change the size of the picture in the document by changing the size in the picture file.

## Build ##
* Compile: `make`
* Cleanup:
	* `make clean` to remove `output/`, `main.pdf` and `main.synctex.gz`
	* `make cleanup` to remove temporary files only
